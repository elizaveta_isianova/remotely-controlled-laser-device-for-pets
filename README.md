<h1 align="center">Welcome to my project - Remotely controlled laser pet device 👋</h1>

## ✨ Description

The topic of this project is a remotely controlled IoT device for communication with pets. 
The device consists of two components: 
- laser station - a two-axis robot with a laser pointer at the end-effector,
- remote control that reads input from the accelerometer processes it and sends the coordinates to the laser station.

This architecture allows the robot's TCP (Tool Center Point) to follow the human's arm's motions that hold the remote controller. The movement of the TCP is provided by two connected servo motors, one of which moves along the horizontal axis, the second one - along the vertical axis. Motions angles of both motors range from 0 to 180 degrees. A 3D printed case ensures the connection of the servomotor.

<p align="center">
  <img src="photos/0.png?raw=true" height="500" title="hover text">
</p>

## 🔌 Hardware Design

The components used in the project are

1. Nucleo-F446RE,

2. Transistor NPN BC546-C,

3. Wi-Fi module ESP8266, 

4. Laser module 3V 650nm, 

5. MG90S servomotor (2x).


The block diagram of the project is shown below.
<p align="center">
  <img src="photos/1.png?raw=true" height="270" title="hover text">
</p>

The PCB design is shown below.

<p align="center">
  <img src="photos/2.png?raw=true" height="550" title="hover text">
  <img src="photos/3.png?raw=true" height="550" title="hover text">
</p>


## 💻 Software implementation

Communication between the remote control and the laser module is provided via the UDP protocol. Originally
TCP communication protocol was chosen, but it did not meet the requirements for fast communication with animals, and therefore it was considered to be too slow. 
A set of AT commands for setting up communication via the UDP protocol is shown below.

<p align="center">
  <img src="photos/4.png?raw=true" height="270" title="hover text">
</p>

The remote control is equipped with the MPU6050 module (accelerometer + gyroscope). It provides data about the deflection angle from the three Cartesian axes: X, Y, and Z. The microprocessor Nucleo-F446RE then reads this data via I2C protocol.
Then the filtration is applied to the received data. The data recalculation determines the direction in which the hand moves. The coordinates of the laser pointer's next position are then sent to the second microprocessor via Wi-Fi. 

## 👩‍🎓  Author

 **Elizaveta Isianova**

- Github: [@elizaveta_isianova](https://gitlab.com/elizaveta_isianova)

## 🫶 Show your support

Please ⭐️ this repository if this project helped you!

## 📝 License

Copyright © 2021 [Elizaveta Isianova](https://gitlab.com/elizaveta_isianova).<br />
