/*
    Program for Wifi client - remote control for laser.
    Program for reading data from accelerometer and gyroscope and sending it to 
    WiFi hotspot through AT commands. 
    Made for Nucleo-F042F6.
    Author: Elizaveta Isianova
    20.05.21
*/

#include "mbed.h"
#include "MPU6050.h"
#include <stdio.h>
#include <string.h>

Serial pc(PA_2, PA_3,115200); // serial communication with ESP8266
 
 #define D_SDA                  PF_0 
 #define D_SCL                  PF_1 
 
I2C i2c(D_SDA, D_SCL);  // I2C communication with MPU6050
DigitalOut wifi_enable(PA_4);
DigitalOut led_control(PA_1);
 
MPU6050 mpu(0x68);//i2c
 
int16_t ax, ay, az; // accelerometer data from 3 axes: x, y, z  
int16_t gx, gy, gz; // gyroscope data from 3 axes: x, y, z  

/*
    Enables accelerometer and gyroscope module MPU6050. 
*/

void initialization(){
    led_control = 1;
    wait(1);
    pc.printf("MPU6050 test\r\n\n");
    pc.printf("MPU6050 initialize \r\n");
 
    mpu.initialize();
    
    pc.printf("MPU6050 testConnection \r\n");
 
    bool mpu6050TestResult = mpu.testConnection();
    if(mpu6050TestResult) {
        pc.printf("MPU6050 test passed \r\n");
    } else {
        pc.printf("MPU6050 test failed \r\n");
    }
    led_control = 0;
    
} 

/*
    Enables ESP8266 Wifi module. 
    By AT commands sets Wifi module into client mode.
*/

void start_wifi(){
    wifi_enable = 1;
    pc.printf("AT+RST\r\n");
    wait_ms(1000);
    pc.printf("AT\r\n");
    wait_ms(1000);
    pc.printf("AT+CWMODE=1\r\n");
    wait_ms(2000);
}

/*
    Connects to Wifi hotspot by AT comands.
    The process of connection repeats until the reply "OK" recieved and
    the connection is established.
*/

void connect_to_hotspot(){
    char buffer[100];
    bool should_exit = 0;
    while(1){
            pc.printf("AT+CWJAP=\"HOTSPOT\",\"\"\r\n");
            for (int i = 0; i<1000000; i++) {
                if (pc.readable()) {
                    pc.gets(buffer, 50);
                    buffer[2] = '\0';
                    
                    if (strcmp ("OK",buffer) == 0){
                        should_exit = 1;
                        break;
                    }
                }
            }
            if (should_exit) {
                break;
            }
        }
}

/*
    Creates UDP connection with Wifi to a server by AT comands.
    The process of connection repeats until the reply "OK" recieved and
    the connection is established.
*/

void connect_to_tcp_server(){
    char buffer[100];
    bool should_exit = 0;
    while(1){
        pc.printf("AT+CIPSTART=0,\"UDP\",\"192.168.4.1\",333,1112,2\r\n");
        for (int i = 0; i<1000000; i++) {
            if (pc.readable()) {
                pc.gets(buffer, 50);
                buffer[2] = '\0';
                
                if (strcmp ("OK",buffer) == 0){
                    should_exit = 1;
                    break;
                }
            }
        }
        if (should_exit) {
            break;
        }
    }
}

/*
    Checks if the connection with the server wasn't lost. 
    If it was lost: returns 1, else: 0.
*/
bool wait_for_reply_ok(){
    bool connection_is_closed = 0;
    char buffer[100];
    while(1){
        if (pc.readable()) {
            pc.gets(buffer, 50);
            buffer[6] = '\0';
            if (strcmp ("CLOSED",buffer) == 0){
                connection_is_closed = 1;
                return connection_is_closed;
            }
            
            buffer[2] = '\0';
            if (strcmp ("OK",buffer) == 0){
                return connection_is_closed;
            }
        }
    }
}

/*
    Changes data recieved from accelerometer into degrees, 
    sends data through UDP connection in format "xxx,yyy", where 
    xxx is data from x axis: degrees in range (0; 180),
    yyy is data from y axis: degrees in range (0; 180).
*/

void send_data_from_accelerometer(int ax, int ay){
    ax = (ax + 14800)/180;
    ay = (ay + 16600)/180;
    if (ax<0) {ax = 1;}
    if (ay<0) {ay = 1;}
    if (ax>179) {ax = 179;}
    if (ay>179) {ay = 179;}
    
    if(ax<10){
        pc.printf("00");
        pc.printf("%d",ax);
    } else if (ax < 100) {
        pc.printf("0");
        pc.printf("%d",ax);
    } else {
        pc.printf("%d",ax);
    }
    pc.printf(",");
    
    if(ay<10){
        pc.printf("00");
        pc.printf("%d",ay);
    } else if (ay < 100) {
        pc.printf("0");
        pc.printf("%d",ay);
    } else {
        pc.printf("%d",ay);
    }
    pc.printf("\r\n");
}

/*
    Checks if the data was sent successfully. 
    If the data was sent successfully, returns 0, 
    else if the connection was lost, returns: 1.
*/

bool wait_for_reply_send_ok(){
    bool connection_is_closed = 0;
    char buffer[100];
    while(1){
        if (pc.readable()) {
            pc.gets(buffer, 50);
            buffer[7] = '\0';
            
            if (strcmp ("SEND OK",buffer) == 0){
                return connection_is_closed;
            }
            buffer[6] = '\0';
            if (strcmp ("CLOSED",buffer) == 0){
                connection_is_closed = 1;
                return connection_is_closed;
            }
        }
    }
}

int main()
{
    initialization();
    
    while(1) {
        
        start_wifi();
    
        connect_to_hotspot();
        pc.printf("AT+CIPMUX=1\r\n");
        wait_ms(100);
        connect_to_tcp_server();
    
        bool should_continue = 1;
        char buffer[100];
        while(1){
            should_continue = 1;
            mpu.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);
            pc.printf("AT+CIPSEND=0,7,\"192.168.4.1\",333\r\n");
            if (wait_for_reply_ok()) {  // if the connection is CLOSED
                break;
            }
            wait_ms(100);
                     
            send_data_from_accelerometer(ax, ay);
            if (wait_for_reply_send_ok()) { // if the connection is CLOSED
                break;
            }
                      
            wait_ms(500);
            
            if (!should_continue) {
                
                pc.printf("Connection lost!\r\n");
                break;
            }
        }
    }
}
