/*
    Program for Wifi hotspot - laser station.
    Program for receiving data from accelerometer (by WiFi), calculating duty for PWM signal
    to motors.
    Made for Nucleo-F446RE.
    Author: Elizaveta Isianova
    20.05.21
*/

#include "mbed.h"
#include <stdlib.h>

Serial pc(PA_2, PA_3, 115200); // serial communication with ESP8266
PwmOut servo_vertical(PB_3); 
PwmOut servo_horizontal(PB_4);
DigitalOut motor_enable(PB_5);
DigitalOut laser(PA_8);
DigitalOut wifi_enable(PA_10);

/*
    Initialization of WiFi module. Creation of UDP server.
*/

void initialize_wifi(){
    wifi_enable = 1;
    
    pc.printf("AT+RST\r\n");
    wait_ms(1000);
    pc.printf("AT\r\n");
    wait_ms(1000);
    pc.printf("AT+CWMODE=3\r\n");
    wait_ms(2000);
    pc.printf("AT+CIPMUX=1\r\n");
    wait_ms(2000);
    pc.printf("AT+CIPSTART=0,\"UDP\",\"192.168.4.1\",333,333,2\r\n");
    wait_ms(100);
    pc.printf("AT+CIFSR\r\n");
    wait_ms(100);
    pc.printf("AT+CIPSTATUS\r\n");
    wait_ms(100);
    motor_enable = 1;
    laser = 1;
}

/*
    Calculates the duty for motors from angle received from accelerometer.
*/

float calculate_duty(int angle){
    float duty = 0.0275f+angle*0.00051f; 
        if (duty < 0.0277f){
            duty = 0.0277f;
        }
        if (duty > 0.116f){
            duty = 0.116f;
        }
        return duty;
}

/*
    Sets motors to the position, according to the data recieved from accelerometr.
*/

int drive_motor(int x, int previous_x){
    int k1;
    int diff1;
    float duty;
    if (x > previous_x){
        diff1 = x - previous_x;
        k1 = 1;
    } else {
        diff1 = previous_x - x;
        k1 = -1;
    }
    for (int i = 0; i < diff1; i++){
        previous_x = previous_x + k1;
        duty = calculate_duty(previous_x);
        servo_vertical.write(duty); 
        wait_ms(15);
    }
    return previous_x;
}

/*
    Reads data recieved by WiFi from acceleroemtr. X axis
*/
int read_x_data_from_serial(){
    char buffer3[50];
    char buffer5[50];
    char buffer[50];
    pc.gets(buffer, 14); // for \r\n

    pc.gets(buffer3, 10);     // for +IPD
    buffer3[11] = '\0';

    pc.gets(buffer5, 4); // vertical servo 
    buffer5[4] = '\0';
        
    int x = atoi(buffer5);
    x = 180 - x;
    if (x>180){
        x = 180;
    } else if (x < 0) {
        x = 0;
    }
    return x;
}
/*
    Reads data recieved by WiFi from acceleroemtr. Y axis
*/
int read_y_data_from_serial(){
    char buffer7[50];
    char bufferX[50];
    pc.gets(bufferX, 2);  // for ","
    bufferX[2] = '\0';
    
    pc.gets(buffer7, 4); // horizontal servo 
    buffer7[4] = '\0';  
    
    int y = atoi(buffer7);
    if (y>180){
        y = 180;
    } else if (y < 0) {
        y = 0;
    } 
    return y;
}

void starting_movements_of_motors(){
    float duty;
    for (int i = 0; i < 180; i++){
        duty = calculate_duty(i);
        servo_horizontal.write(duty);
        wait_ms(10);
    }
    for (int i = 180; i > 0; i--){
        duty = calculate_duty(i);
        servo_vertical.write(duty); 
        wait_ms(10);
    }
}


int main()
{
    initialize_wifi();

    servo_vertical.period(0.02f); //20 ms
    servo_horizontal.period(0.02f); //20 ms 
    
    float duty;
    int previous_x = 90;
    int previous_y = 90;
    int x;
    int y;

    starting_movements_of_motors();

    wait(1);

    while(1){
        if(pc.readable()) { 
            
            x = read_x_data_from_serial();
            y = read_y_data_from_serial();
            previous_x = drive_motor(x, previous_x);
            previous_y = drive_motor(y, previous_y);

            wait_ms(20);
        }        
    }
}
    
